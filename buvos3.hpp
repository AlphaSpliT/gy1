#ifndef BUVOS2_H
#define BUVOS2_H

#include "buvos.hpp"
#include <cmath>

class Buvos2 : public Buvos {
    bool is_ok();
};

bool Buvos2::is_ok() {
     int * foo = new int[10] {0};

    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            if (this->m[i][j] <= 9) {
                foo[m[i][j]] = 1;
            }
        }
    }

    for (int i = 1; i <= 9; ++i) {
        if (foo[i] != 1) {
            return false;
        }
    }

    for (int i = 1; i < 3; ++i) {
        if (this->sum_col(i) != this->sum_col(0) || this->sum_row(i) != this->sum_col(0)) {
            return false;
        }
    }

    delete[] foo;
    
    int a1 = this->m[0][0] + this->m[1][1] + this->m[2][2];
    int a2 = this->m[0][2] + this->m[1][1] + this->m[2][0];

    if (abs(a1 - a2) != 3) {
        return false;
    }

    return true;
}

#endif