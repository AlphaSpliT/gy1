#include "utils.hpp"

#ifndef BUVOS_H
#define BUVOS_H

class Buvos {
protected:
    int m[3][3];
    int count;
public:
    Buvos();

    void kiir();

    virtual bool is_ok();
    bool is_full();
    bool no_repeat();

    int sum_row(int);
    int sum_col(int);
    int get_count();

    int * operator[](int);
    
    void backtracking(int);
};

Buvos::Buvos() {
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            this->m[i][j] = 0.0f;
        }
    }
    this->count = 0;
}

void Buvos::kiir() {
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            cout << m[i][j] << " ";
        }
        cout << endl;
    }
}

bool Buvos::no_repeat() {
    int * foo = new int[10] {0};
    
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            if (this->m[i][j] <= 9) {
                foo[m[i][j]]++;
            }
        }
    }

    for (int i = 1; i <= 9; ++i) {
        if (foo[i] > 1) {
            return false;
        }
    }

    return true;
}

bool Buvos::is_full() {
    int count = 0;
    
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            count += (m[i][j] != 0 ? 1 : 0);
        }
    }

    return count == 9;
}

bool Buvos::is_ok() {
    int * foo = new int[10] {0};

    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            if (this->m[i][j] <= 9) {
                foo[m[i][j]] = 1;
            }
        }
    }

    for (int i = 1; i <= 9; ++i) {
        if (foo[i] != 1) {
            return false;
        }
    }

    for (int i = 1; i < 3; ++i) {
        if (this->sum_col(i) != this->sum_col(0) || this->sum_row(i) != this->sum_col(0)) {
            return false;
        }
    }

    delete[] foo;

    return true;
}

int Buvos::sum_row(int row) {
    if (row < 0 && row >= 3) {
        return 0.0f;
    }

    int sum = 0.0f;

    for (int i = 0; i < 3; ++i) {
        sum += this->m[row][i];
    }

    return sum;
}

int Buvos::sum_col(int col) {
    if (col < 0 && col >= 3) {
        return 0.0f;
    }

    int sum = 0.0f;

    for (int i = 0; i < 3; ++i) {
        sum += this->m[i][col];
    }

    return sum;
}

int * Buvos::operator[](int key) {
    if (key < 0 || key >= 3) {
        return nullptr;
    }

    return this->m[key];
}

void Buvos::backtracking(int k = 0) {
    if (k < 9) {
        for (int i = 1; i <= 9; ++i) {
            (*this)[k / 3][k % 3] = i;
            if (this->no_repeat()) {
                backtracking(k + 1);
            }
        }
        (*this)[k / 3][k % 3] = 0;
    
    } else if (k == 9) {
        if (this->is_ok()) {
            this->count++;
        }
    }
}

int Buvos::get_count() {
    return this->count;
}

#endif