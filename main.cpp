#include "buvos2.hpp"
#include "buvos3.hpp"

// Nev: Albert Ferencz
// azonosito: afim1689

int main() {
    Buvos mat;
    mat.backtracking();
    cout << mat.get_count() << endl;

    Buvos2 mat2;
    mat2.backtracking();
    cout << mat2.get_count() << endl;

    Buvos2 mat3;
    mat3.backtracking();
    cout << mat3.get_count() << endl;
}